import cv2
import matplotlib.pyplot as plt
path = '../images'

img=cv2.imread(path+'/Section-3/cube-02.jpg')



p1_points = []
fig = plt.figure(figsize=(10,10))

def onclick(event):
    p1_points.append([event.xdata, event.ydata])
    c1 = plt.Circle([event.xdata, event.ydata], 0.02,)
#     plt.show()
    event.canvas.figure.gca().add_artist(c1)
    event.canvas.figure.show()
    

cid = fig.canvas.mpl_connect('button_press_event', onclick)
# print(p1_points)

imgplot = plt.imshow(img)
plt.show()

print(p1_points)